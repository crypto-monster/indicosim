import { HttpModuleOptions } from "@nestjs/axios";
import { HttpModuleOptionsFactory, Inject, NotImplementedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { REQUEST } from "@nestjs/core";
import { Request } from "express";

export abstract class AbstractHttpClientConfig implements HttpModuleOptionsFactory {

    constructor(
        @Inject(REQUEST) protected readonly request: Request,
                protected readonly configService: ConfigService
    ) { }

    public createHttpOptions(): HttpModuleOptions {
        return {
            baseURL: this.getHost(),
            headers: this.getHeaders(),
            timeout: this.getTimeout(),
            maxRedirects: this.getMaxRedirects(),
            maxBodyLength: this.getFormDataMaxSize()
        };
    }

    protected getHeaders(): Record<string, string> {
        const customHeaders = this.getCustomHeaders();

        let headers = {
            'Accept-Encoding': 'deflate, gzip;q=1.0, *;q=0.5'
        }

        return {
            ...headers,
            ...customHeaders
        };
    }

    protected forwardUserRoles(): boolean {
        return false;
    }

    protected getHost(): string {
        throw new NotImplementedException('you must implement getHost()');
    }

    protected getTimeout(): number {
        throw new NotImplementedException('you must implement getTimeout()');
    }

    protected getCustomHeaders(): Record<string, unknown> {
        return;
    }

    private getFormDataMaxSize() {
        return this.configService.get('FORM_DATA_MAX_SIZE');
    }

    private getMaxRedirects() {
        return this.configService.get('HTTP_MAX_REDIRECTS');
    }

}