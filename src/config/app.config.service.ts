import { Injectable } from "@nestjs/common";
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfigService {

    public static port: number;

    constructor(private readonly configService: ConfigService) {
        AppConfigService.port = this.configService.get('SERVICE_PORT')
    }

}