import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
import { AppConfigService } from "./config/app.config.service";
import { PrismaService } from './config/prisma/prisma.service';
import { WhatsappModule } from "./feature/whatsapp/whatsapp.module";
import { WebHookModule } from "./feature/webhook/webhook.module";
import { HealthModule } from "./feature/health/health.module";
import { MetricModule } from './feature/metric/metric.module';
import { SearchModule } from "./feature/search/search.module";
import { UserIndicosimModule } from "./feature/user-indicosim/user.indicosim.module";
import { CityModule } from "./feature/city/city.module";
import {ReputationModule} from "./feature/reputation/reputation.module";
import {CategoryModule} from "./feature/category/category.module";
import {ProfessionalModule} from "./feature/professional/professional.module";

@Module({
  imports: [
      ConfigModule.forRoot({
          isGlobal: true,
      }),
      WhatsappModule,
      SearchModule,
      WebHookModule,
      HealthModule,
      MetricModule,
      UserIndicosimModule,
      CityModule,
      ReputationModule,
      CategoryModule,
      ProfessionalModule
  ],
  controllers: [],
  providers: [
      AppConfigService,
      PrismaService,
  ],
  exports: [
      ConfigModule,
  ]
})
export class AppModule {}
