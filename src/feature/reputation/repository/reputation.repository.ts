import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";

@Injectable()
export class ReputationRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {}

}