import { Module } from "@nestjs/common";
import { ReputationRepository } from "./repository/reputation.repository";
import { ReputationService } from "./service/reputation.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        ReputationRepository,
        ReputationService,
        PrismaService,
    ],
    exports: [
        ReputationService
    ],
    controllers: []
})
export class ReputationModule { }
