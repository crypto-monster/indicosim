export class Search {

    readonly uuid;
    readonly entityUuid;
    readonly entityType;
    readonly name;
    readonly soundexName;
    readonly fatherUuid;


    constructor(uuid, entityUuid, entityType, name, soundexName, fatherUuid) {
        this.uuid = uuid;
        this.entityUuid = entityUuid;
        this.entityType = entityType;
        this.name = name;
        this.soundexName = soundexName;
        this.fatherUuid = fatherUuid;
    }
}