import { Injectable } from "@nestjs/common";
import { SearchRepository } from "../repository/search.repository";
import { Search } from "../model/search.model";

@Injectable()
export class SearchService {

    constructor(
        private readonly searchRepository: SearchRepository
    ) {}

    async findBySoundexNameAndEntityType(soundexName: string, entityType: string): Promise<Search> {
        return await this.searchRepository.findBySoundexNameAndEntityType(soundexName, entityType);
    }

    async findByUuid(uuid: string): Promise<Search> {
        return await this.searchRepository.findByUuid(uuid);
    }
}