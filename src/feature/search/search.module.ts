import { Module } from "@nestjs/common";
import { SearchRepository } from "./repository/search.repository";
import { SearchService } from "./service/search.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        SearchRepository,
        SearchService,
        PrismaService,
    ],
    exports: [
        SearchService
    ],
    controllers: []
})
export class SearchModule { }
