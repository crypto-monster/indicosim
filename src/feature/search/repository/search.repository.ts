import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";
import { Search } from "../model/search.model";

@Injectable()
export class SearchRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {}

    async findBySoundexNameAndEntityType(soundexName: string, entityType: string): Promise<Search> {
        const search = await this.prismaService.search.findFirst({
            where: {
                soundex_name: soundexName,
                entity_type: entityType
            }
        });

        return new Search(
            search.uuid,
            search.entity_uuid,
            search.entity_type,
            search.name,
            search.soundex_name,
            search.father_uuid
        );
    }

    async findByUuid(uuid: string): Promise<Search> {
        const search = await this.prismaService.search.findUnique({
            where: {
                uuid: uuid
            }
        });

        return new Search(
            search.uuid,
            search.entity_uuid,
            search.entity_type,
            search.name,
            search.soundex_name,
            search.father_uuid
        );
    }
}