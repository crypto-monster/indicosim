import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";
import { City, CityBD } from "../model/city.model";

@Injectable()
export class CityRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {}

    async findAll(): Promise<City[]> {
        const cities = await this.prismaService.$queryRaw<CityBD[]>`
            SELECT *
            FROM city
        `;

        return cities.map(city => new City(city.uuid, city.name, city.soundex_name, city.ddd, city.country,
            city.last_modified_date, city.created_date));
    }

}