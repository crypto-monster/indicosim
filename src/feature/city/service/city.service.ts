import { Injectable } from "@nestjs/common";
import { CityRepository } from "../repository/city.repository";
import { City } from "../model/city.model";

@Injectable()
export class CityService {

    constructor(
        private readonly cityRepository: CityRepository
    ) {}

    async findAll(): Promise<City[]> {
        return this.cityRepository.findAll();
    }

}