export class CityBD {

    readonly uuid: string
    readonly name: string
    readonly soundex_name: string
    readonly ddd: string
    readonly country: string
    readonly last_modified_date: string
    readonly created_date: string

}

export class City {

    readonly uuid: string
    readonly name: string
    readonly soundexName: string
    readonly ddd: string
    readonly country: string
    readonly lastModifiedDate: string
    readonly createdDate: string

    constructor(uuid: string, name: string, soundexName: string, ddd: string, country: string, lastModifiedDate: string, createdDate: string) {
        this.uuid = uuid;
        this.name = name;
        this.soundexName = soundexName;
        this.ddd = ddd;
        this.country = country;
        this.lastModifiedDate = lastModifiedDate;
        this.createdDate = createdDate;
    }
}