import { Module } from "@nestjs/common";
import { CityRepository } from "./repository/city.repository";
import { CityService } from "./service/city.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        CityRepository,
        CityService,
        PrismaService,
    ],
    exports: [
        CityService
    ],
    controllers: []
})
export class CityModule { }
