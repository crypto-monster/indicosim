export class MetricDTO {

    readonly uuid: string;
    readonly client_uuid: string;
    readonly user_indicosim_uuid: string;
    readonly phone_number: string;
    readonly created_date: string;

}