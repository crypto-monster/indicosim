import { Injectable } from "@nestjs/common";
import { PrismaService } from "../../../config/prisma/prisma.service";
import { MetricEntity } from "../entity/metric.entity";

@Injectable()
export class ProfessionalMetricRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {
    }

    async save(metricEntity: MetricEntity) {
        return await this.prismaService.professional_metric.create({
            data: metricEntity
        })
    }

}