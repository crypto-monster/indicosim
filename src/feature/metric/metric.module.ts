import { Module } from "@nestjs/common";
import { CategoryMetricService } from "./service/category-metric.service";
import { ProfessionalMetricService } from "./service/professional-metric.service";
import { ServiceMetricService } from "./service/service-metric.service";
import { CategoryMetricRepository } from "./repository/category-metric.repository";
import { ProfessionalMetricRepository } from "./repository/professional-metric.repository";
import { ServiceMetricRepository } from "./repository/service-metric.repository";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        PrismaService,
        CategoryMetricService,
        ProfessionalMetricService,
        ServiceMetricService,
        CategoryMetricRepository,
        ProfessionalMetricRepository,
        ServiceMetricRepository
    ],
    controllers: [],
    exports: [
        CategoryMetricService,
        ProfessionalMetricService,
        ServiceMetricService
    ]
})
export class MetricModule { }
