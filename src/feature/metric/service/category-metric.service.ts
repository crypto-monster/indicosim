import { Injectable } from "@nestjs/common";
import { MetricDTO } from "../dto/metric.dto";
import { CategoryMetricRepository } from "../repository/category-metric.repository";

@Injectable()
export class CategoryMetricService {

    constructor(
        private readonly categoryMetricRepository: CategoryMetricRepository
    ) {
    }

    async save(metricDTO: MetricDTO) {
        return await this.categoryMetricRepository.save(metricDTO);
    }

}