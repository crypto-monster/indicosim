import { Injectable } from "@nestjs/common";
import { MetricDTO } from "../dto/metric.dto";
import { ProfessionalMetricRepository } from "../repository/professional-metric.repository";

@Injectable()
export class ProfessionalMetricService {

    constructor(
        private readonly professionalMetricRepository: ProfessionalMetricRepository
    ) {
    }

    async save(metricDTO: MetricDTO) {
        return await this.professionalMetricRepository.save(metricDTO);
    }

}