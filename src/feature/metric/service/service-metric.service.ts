import { Injectable } from "@nestjs/common";
import { MetricDTO } from "../dto/metric.dto";
import { ServiceMetricRepository } from "../repository/service-metric.repository";

@Injectable()
export class ServiceMetricService {

    constructor(
        private readonly serviceMetricRepository: ServiceMetricRepository
    ) {
    }

    async save(metricDTO: MetricDTO) {
        return await this.serviceMetricRepository.save(metricDTO);
    }

}