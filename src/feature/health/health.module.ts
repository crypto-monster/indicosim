import { Module } from "@nestjs/common";
import { HealthController } from "./controller/health.controller";

@Module({
    imports: [],
    providers: [],
    controllers: [
        HealthController
    ],
    exports: []
})
export class HealthModule { }
