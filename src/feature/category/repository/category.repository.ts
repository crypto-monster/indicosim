import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";
import { Category } from "../model/category.model";

@Injectable()
export class CategoryRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {}

    async findByUuid(uuid: string): Promise<Category> {
        const category = await this.prismaService.category.findUnique({
            where: {
                uuid: uuid
            }
        });

        return new Category(
            category.uuid,
            category.father_uuid,
            category.name,
            category.soundex_name,
            category.has_professional,
            category.last_modified_date,
            category.created_date
        );
    }

    async findByFatherName(fatherName: string): Promise<Category[]> {
      
        const categories = await this.prismaService.$queryRaw<Category[]>`
            select *
            from category 
            where father_name= ${fatherName}::text`;

        return categories;
    }

}