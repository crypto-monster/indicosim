import { Injectable } from "@nestjs/common";
import { CategoryRepository } from "../repository/category.repository";
import { Category } from "../model/category.model";

@Injectable()
export class CategoryService {

    constructor(
        private readonly categoryRepository: CategoryRepository
    ) {}

    async findByUuid(uuid: string): Promise<Category> {
        return this.categoryRepository.findByUuid(uuid);
    }
    
    async findByFatherName(fatherName: string): Promise<Category[]> {
        return this.categoryRepository.findByFatherName(fatherName);
    }

}