import { Module } from "@nestjs/common";
import { CategoryRepository } from "./repository/category.repository";
import { CategoryService } from "./service/category.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        CategoryRepository,
        CategoryService,
        PrismaService,
    ],
    exports: [
        CategoryService
    ],
    controllers: []
})
export class CategoryModule { }
