export class Category {

    readonly uuid: string;
    readonly fatherUuid: string;
    readonly name: string;
    readonly soundexName: string;
    readonly hasProfessional: boolean;
    readonly lastModifiedDate: string;
    readonly createdDate: string;
    readonly professionals: any;
    readonly description: string;
    readonly fatherName: string;


    constructor(uuid: string, fatherUuid: string, name: string, soundexName: string, hasProfessional: boolean, lastModifiedDate: string, createdDate: string) {
        this.uuid = uuid;
        this.fatherUuid = fatherUuid;
        this.name = name;
        this.soundexName = soundexName;
        this.hasProfessional = hasProfessional;
        this.lastModifiedDate = lastModifiedDate;
        this.createdDate = createdDate;
    }
}