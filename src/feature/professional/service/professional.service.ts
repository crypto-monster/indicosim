import { Injectable } from "@nestjs/common";
import { ProfessionalRepository } from "../repository/professional.repository";
import { Professional } from "../model/professional.model";

@Injectable()
export class ProfessionalService {

    constructor(
        private readonly professionalRepository: ProfessionalRepository
    ) {}

    async findByCategory(categoryUuid: string): Promise<Professional[]> {
        return this.professionalRepository.findByCategory(categoryUuid);
    }

    async findByUuid(uuid: string): Promise<Professional[]> {
        return this.professionalRepository.findByUuid(uuid);
    }

}