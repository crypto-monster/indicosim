export class ProfessionalBD {

    readonly uuid: string;
    readonly name: string;
    readonly fantasy_name: string;
    readonly phone_number: string;
    readonly address: string;
    readonly document_number: string;
    readonly page_url: string;
    readonly disabled: boolean;
    readonly last_modified_date: string;
    readonly created_date: string;

}

export class Professional {

    readonly uuid: string;
    readonly name: string;
    readonly fantasyName: string;
    readonly phoneNumber: string;
    readonly address: string;
    readonly documentNumber: string;
    readonly pageUrl: string;
    readonly disabled: boolean;
    readonly lastModifiedDate: string;
    readonly createdDate: string;

    constructor(uuid: string, name: string, fantasyName: string, phoneNumber: string, address: string, documentNumber: string, pageUrl: string, disabled: boolean, lastModifiedDate: string, createdDate: string) {
        this.uuid = uuid;
        this.name = name;
        this.fantasyName = fantasyName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.documentNumber = documentNumber;
        this.pageUrl = pageUrl;
        this.disabled = disabled;
        this.lastModifiedDate = lastModifiedDate;
        this.createdDate = createdDate;
    }
}