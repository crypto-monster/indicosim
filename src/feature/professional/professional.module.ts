import { Module } from "@nestjs/common";
import { ProfessionalRepository } from "./repository/professional.repository";
import { ProfessionalService } from "./service/professional.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        ProfessionalRepository,
        ProfessionalService,
        PrismaService,
    ],
    exports: [
        ProfessionalService
    ],
    controllers: []
})
export class ProfessionalModule { }
