import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";
import {
    Professional,
    ProfessionalBD
} from "../model/professional.model";

@Injectable()
export class ProfessionalRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) {}

    async findByCategory(categoryUuid: string): Promise<Professional[]> {
        const professionals = await this.prismaService.$queryRaw<ProfessionalBD[]>`
            select * 
            from professional p 
            join professional_category pc on pc.professional_uuid = p.uuid and 
            pc.category_uuid  = ${categoryUuid}::text
        `;

        return professionals.map(professional =>
            new Professional(
                professional.uuid,
                professional.name,
                professional.fantasy_name,
                professional.phone_number,
                professional.address,
                professional.document_number,
                professional.page_url,
                professional.disabled,
                professional.last_modified_date,
                professional.created_date
            )
        );
    }

    async findByUuid(uuid: string): Promise<Professional[]> {
        const professionals = await this.prismaService.$queryRaw<ProfessionalBD[]>`
            select * 
            from professional 
            where uuid  = ${uuid}::text`;

        return professionals.map(professional =>
            new Professional(
                professional.uuid,
                professional.name,
                professional.fantasy_name,
                professional.phone_number,
                professional.address,
                professional.document_number,
                professional.page_url,
                professional.disabled,
                professional.last_modified_date,
                professional.created_date
            )
        );
    }


}