import { Injectable } from "@nestjs/common";
import { MessageReceive } from "../dto/message.receive";
import { WhatsappClient } from "../../whatsapp/client/whatsapp.client";
import { ActionMessage, BodyMessage, ButtonMessage, ContactMessage, FooterMessage, HeaderMessage, InteractiveMessage, Message, NameMessage, PhoneMessage, ReplyMessage, RowMessage, SectionMessage, TextMessage, UrlMessage } from "../../whatsapp/model";
import { ExtractNumberContactMessage } from "src/common/util/extract.number.contact.message";
import { ProfessionalService } from "src/feature/professional/service/professional.service";
import { UserIndicosimService } from "src/feature/user-indicosim/service/user.indicosim.service";
import { ArrayValidation } from "src/common/util/array.valiation";

@Injectable()
export class MessageListButtonService {

    constructor(
        private readonly whatsappClient: WhatsappClient,
        private readonly professionalService: ProfessionalService,
        private readonly userIndicoSimService: UserIndicosimService,
    ) { }

    async message(message: MessageReceive) {
        const numberContact = ExtractNumberContactMessage.extract(message);
        const chat = await this.userIndicoSimService.findByPhoneNumberAndCreatedDateAndTimeWindowChat(numberContact);

        if (ArrayValidation.isEmpty(chat)) {
            return;
        }

        const isProfessionalByIncludesIntoDescription = message.entry[0]
            .changes[0]
            .value
            .messages[0]
            .interactive
            .list_reply
            .description.includes('Reputação');

       
        if (isProfessionalByIncludesIntoDescription) {
            const professionalUuid = message.entry[0].changes[0].value.messages[0].interactive.list_reply.id;
            const professionals = await this.professionalService.findByUuid(professionalUuid);

            if (ArrayValidation.isEmpty(professionals)) {
                return;
            }

            await this.whatsappClient.sendMessage(new Message(
                numberContact,
                'contacts',
                undefined,
                undefined,
                undefined,
                [
                    new ContactMessage(
                        new NameMessage(professionals[0].name, professionals[0].name, professionals[0].name),
                        [
                            new PhoneMessage(professionals[0].phoneNumber, professionals[0].phoneNumber, 'WORK')
                        ],
                        [
                            new UrlMessage(professionals[0].pageUrl, 'WORK')
                        ]
                    )
                ],
            ));
        } else {
            const categoryUuid = message.entry[0].changes[0].value.messages[0].interactive.list_reply.id;
            const professionals = await this.professionalService.findByCategory(categoryUuid);

            if (ArrayValidation.isEmpty(professionals)) {
                return;
            }
           
            const rowMessages = professionals?.map(professional => 
                new RowMessage(professional.uuid, professional.name, 'Preço: Médio | Reputação: Excelente')
            )

            await this.whatsappClient.sendMessage(new Message(
                numberContact,
                'interactive',
                undefined,
                new InteractiveMessage(
                    'list',
                    new BodyMessage('Escolha o profissional:'),
                    new ActionMessage(
                        undefined,
                        'Listar Profissionais',
                        [
                            new SectionMessage(
                                message.entry[0].changes[0].value.messages[0].interactive.list_reply.title,
                                [
                                   ...rowMessages
                                ]
                            )]
                    ),
                    new FooterMessage('Você poderá voltar para o ínicio quando quiser.'),
                    new HeaderMessage('Para o meu lar')
                ),
            ));
        }
    }
}