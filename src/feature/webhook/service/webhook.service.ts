import { Injectable } from "@nestjs/common";
import { MessageReceive } from "../dto/message.receive";
import { DefineTypeOfMessage } from "../../../common/util/define.type.of.message";
import { WhatsappClient } from "../../whatsapp/client/whatsapp.client";
import { ActionMessage, BodyMessage, ButtonMessage, ContactMessage, FooterMessage, HeaderMessage, InteractiveMessage, Message, NameMessage, PhoneMessage, ReplyMessage, RowMessage, SectionMessage, TextMessage, UrlMessage } from "../../whatsapp/model";
import { ExtractNumberContactMessage } from "../../../common/util/extract.number.contact.message";
import { ChatTextService } from "./chat.text.service";
import { MessageListButtonService } from "./message.list.button.service";
import { MessageButtonService } from "./message.button.service";

@Injectable()
export class WebhookService {

    constructor(
        private readonly whatsappClient: WhatsappClient,
        private readonly chatTextService: ChatTextService,
        private readonly messageListButtonService: MessageListButtonService,
        private readonly messageButtonService: MessageButtonService,
    ) { }

    async message(message: MessageReceive) {
        const messageType = DefineTypeOfMessage.define(message);
        
        switch (messageType) {
            case "message-text": {
                await this.chatTextService.message(message);
                break;
            }
            case "message-list-button": {
                await this.messageListButtonService.message(message);
                break;
            }
            case "message-button": {
                await this.messageButtonService.message(message);
                break;
            }
            default: {
                console.log('mensagem não encontrada')
                break;
            }

        }
    }
}
