import { Injectable } from "@nestjs/common";
import { MessageReceive } from "../dto/message.receive";
import { ExtractNumberContactMessage } from "../../../common/util/extract.number.contact.message";
import { ExtractTextMessage } from "../../../common/util/extract.text.message";
import { EncodeSoundex } from "../../../common/soundex/encode.soundex";
import { ArrayValidation } from "../../../common/util/array.valiation";
import { MessageUtil } from "../util/message.util";
import {
    ActionMessage,
    BodyMessage,
    ButtonMessage,
    ContactMessage,
    InteractiveMessage,
    Message,
    NameMessage,
    PhoneMessage,
    ReplyMessage,
    TextMessage, UrlMessage
} from "../../whatsapp/model";
import { City } from "../../city/model/city.model";
import { Professional } from "../../professional/model/professional.model";
import { WhatsappClient } from "../../whatsapp/client/whatsapp.client";
import { CategoryService } from "../../category/service/category.service";
import { ProfessionalService } from "../../professional/service/professional.service";
import { CityService } from "../../city/service/city.service";
import { SearchService } from "../../search/service/search.service";
import { UserIndicosimService } from "../../user-indicosim/service/user.indicosim.service";
import { InterestArea } from "../enum/interest.area";
import { Response } from "../../whatsapp/client/model/response.message";
import {
    addHours,
    format
} from "date-fns";
import { UserIndicosim } from "src/feature/user-indicosim/model/user.indicosim.model";
import {v4 as uuidv4} from 'uuid';

@Injectable()
export class ChatTextService {

    constructor(
        private readonly whatsappClient: WhatsappClient,
        private readonly searchService: SearchService,
        private readonly userIndicoSimService: UserIndicosimService,
        private readonly categoryService: CategoryService,
        private readonly professionalService: ProfessionalService,
        private readonly cityService: CityService,
    ) { }

    async message(message: MessageReceive) {
        const numberContact = ExtractNumberContactMessage.extract(message);
        const messageReceived = ExtractTextMessage.extract(message);
        const chatExists = await this.userIndicoSimService.findByPhoneNumberAndCreatedDateAndTimeWindowChat(numberContact);

        if (ArrayValidation.isEmpty(chatExists) || !chatExists[0].cityUuid) {
            const cities = await this.cityService.findAll();
            await this.saveChat(numberContact);
            await this.whatsappClient.sendMessage(new Message(numberContact, 'text', new TextMessage(false, 'Olá somos o indico sim, a maior plataforma de indicações da américa latina. ')));
            await this.sendMessageCity(numberContact, cities);
            return;
        }

        if (!ArrayValidation.isEmpty(chatExists)) {
            const messageReceivedEncodedSoundex = EncodeSoundex.encode(messageReceived);
            const search = await this.searchService.findBySoundexNameAndEntityType(messageReceivedEncodedSoundex, 'SERVICE');

            if (!search) {
                await this.sendMessageCategories(numberContact);
                return;
            }

            const searchCategory = await this.searchService.findByUuid(search.fatherUuid)

            const category = await this.categoryService.findByUuid(searchCategory.entityUuid);

            if (!category || !category.hasProfessional)  {
                await this.sendMessageCategories(numberContact);
                return;
            }

            const professionals = await this.professionalService.findByCategory(category.uuid);

            if (ArrayValidation.isEmpty(professionals)) {
                await this.sendMessageCategories(numberContact);
                return;
            }

            await this.sendMessageProfessionals(numberContact, professionals);
            return;
        }
    }

    private async saveChat(numberContact: string) {
        const now = new Date();
        const createdDate = format(now, 'yyyy-MM-dd');
        const dateTimeWindow = format(now, 'yyyy-MM-dd HH:mm:ss');

        const userIndicosim = new UserIndicosim(uuidv4(), numberContact, createdDate, undefined, undefined, dateTimeWindow, undefined);
        await this.userIndicoSimService.save(userIndicosim);
    }

    private async sendMessageCategories(numberContact: string) {
        await this.whatsappClient.sendMessage(new Message(
            numberContact,
            'interactive',
            undefined,
            new InteractiveMessage(
                'button',
                new BodyMessage(MessageUtil.AREA_OF_INTEREST),
                new ActionMessage([
                    new ButtonMessage('reply', new ReplyMessage('001', InterestArea.CAR)),
                    new ButtonMessage('reply', new ReplyMessage('002', InterestArea.HOME))
                ])
            ),
        ));
    }

    private async sendMessageCity(numberContact: string, cities: City[]) {
        await this.whatsappClient.sendMessage(new Message(
                numberContact,
                'interactive',
                undefined,
                new InteractiveMessage(
                    'button',
                    new BodyMessage(MessageUtil.ASK_THE_CITY),
                    new ActionMessage(cities.map(city => new ButtonMessage('reply', new ReplyMessage(city.uuid, city.name))))
                ),
            ));
    }

    public async sendMessageProfessionals(numberContact: string, professionals: Professional[]): Promise<Response> {
        return await this.whatsappClient.sendMessage(new Message(
            numberContact,
            'contacts',
            undefined,
            undefined,
            undefined,
            professionals.map(professional =>
                new ContactMessage(
                    new NameMessage(professional.name, professional.name, professional.name),
                    [
                        new PhoneMessage(professional.phoneNumber, professional.phoneNumber, 'WORK')
                    ],
                    [
                        new UrlMessage(professional.pageUrl, 'WORK')
                    ]
                )
            )
        ));
    }

}