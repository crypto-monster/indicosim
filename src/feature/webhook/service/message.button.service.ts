import { Injectable } from "@nestjs/common";
import { ExtractNumberContactMessage } from "src/common/util/extract.number.contact.message";
import { ArrayValidation } from "src/common/util/array.valiation";
import { UserIndicosimService } from "../../user-indicosim/service/user.indicosim.service";
import { WhatsappClient } from "../../whatsapp/client/whatsapp.client";
import { ActionMessage, BodyMessage, ButtonMessage, FooterMessage, HeaderMessage, InteractiveMessage, Message, ReplyMessage, RowMessage, SectionMessage } from "../../whatsapp/model";
import { MessageReceive } from "../dto/message.receive";
import { convertValueToLastMessageType, InterestArea } from "../enum/interest.area";
import { CategoryService } from "src/feature/category/service/category.service";

@Injectable()
export class MessageButtonService {

    constructor(
        private readonly whatsappClient: WhatsappClient,
        private readonly userIndicoSimService: UserIndicosimService,
        private readonly categoryService: CategoryService
    ) { }

    async message(message: MessageReceive) {
        const numberContact = ExtractNumberContactMessage.extract(message);
        const userIndicosim = await this.userIndicoSimService.findByPhoneNumberAndCreatedDateAndTimeWindowChat(numberContact);

        if (ArrayValidation.isEmpty(userIndicosim)) {
            console.log('chat para o numberContact '+numberContact+' não encontrado');
            return;
        }

        const messageTitle = message?.entry[0].changes[0].value.messages[0].interactive?.button_reply?.title;

        if (!userIndicosim[0].cityUuid) {
            userIndicosim[0].cityUuid = message?.entry[0].changes[0].value.messages[0].interactive?.button_reply?.id;
            await this.userIndicoSimService.save(userIndicosim[0]);

            return await this.whatsappClient.sendMessage(new Message(
                numberContact,
                'interactive',
                undefined,
                new InteractiveMessage(
                    'button',
                    new BodyMessage(`Ótimo ${messageTitle}!!! Selecione area de interesse:`),
                    new ActionMessage([
                        new ButtonMessage('reply', new ReplyMessage('001', InterestArea.CAR)),
                        new ButtonMessage('reply', new ReplyMessage('002', InterestArea.HOME))
                    ])
                ),
            ));
        }


        const lastMessageType = convertValueToLastMessageType(messageTitle);
        if (lastMessageType === InterestArea.CAR || lastMessageType === InterestArea.HOME) {

            const categories  = await this.categoryService.findByFatherName(lastMessageType);    
            console.log(JSON.stringify(categories))

            const rowMessages = categories?.map(category => 
                new RowMessage(category.uuid, category.name, category.description)
            )
            return await this.whatsappClient.sendMessage(new Message(
                numberContact,
                'interactive',
                undefined,
                new InteractiveMessage(
                    'list',
                    new BodyMessage('Escreva um serviço ou profissional. Caso tenha dúvidas escolha uma opção:'),
                    new ActionMessage(
                        undefined,
                        'Listar Categorias',
                        [
                            new SectionMessage(
                                lastMessageType,
                                [
                                    ...rowMessages,
                                ]
                            )]
                    ),
                    new FooterMessage('Você poderá voltar para o ínicio quando quiser.'),
                    new HeaderMessage(lastMessageType)
                ),
            ));
        }
    }
}
