export class MessageReceive {

    readonly object: string;
    readonly entry: Entry[];

    constructor(object: string, entry: Entry[]) {
        this.object = object;
        this.entry = entry;
    }

}

export class Entry {

    readonly id: string;
    readonly changes: Change[];

    constructor(id: string, changes: Change[]) {
        this.id = id;
        this.changes = changes;
    }

}

export class Change {

    readonly value: Value;
    readonly field: string;

    constructor(value: Value, field: string) {
        this.value = value;
        this.field = field;
    }

}

export class Value {

    readonly messaging_product: string;
    readonly metadata: Metadata;
    readonly contacts: Contact[];
    readonly messages: Message[];

    constructor(messaging_product: string, metadata: Metadata, contacts: Contact[], messages: Message[]) {
        this.messaging_product = messaging_product;
        this.metadata = metadata;
        this.contacts = contacts;
        this.messages = messages;
    }

}

export class Metadata {

    readonly display_phone_number: string;
    readonly phone_number_id: string;

    constructor(display_phone_number: string, phone_number_id: string) {
        this.display_phone_number = display_phone_number;
        this.phone_number_id = phone_number_id;
    }

}

export class Contact {

    readonly profile: Profile;
    readonly wa_id: string;

    constructor(profile: Profile, wa_id: string) {
        this.profile = profile;
        this.wa_id = wa_id;
    }

}

export class Profile {

    readonly name: string;

    constructor(name: string) {
        this.name = name;
    }

}

export class Message {

    readonly from: string;
    readonly id: string;
    readonly timestamp: string;
    readonly text: Text;
    readonly type: string;
    readonly context: Context;
    readonly interactive: Interactive;

    constructor(from: string, id: string, timestamp: string, text: Text, type: string, context: Context, interactive: Interactive) {
        this.from = from;
        this.id = id;
        this.timestamp = timestamp;
        this.text = text;
        this.type = type;
        this.context = context;
        this.interactive = interactive;
    }

}

export class Text {

    readonly body: string;

    constructor(body: string) {
        this.body = body;
    }

}

export class Context {

    readonly from: string;
    readonly id: string;

    constructor(from: string, id: string) {
        this.from = from;
        this.id = id;
    }

}

export class Interactive {

    readonly type: string;
    readonly button_reply: ButtonReply;
    readonly list_reply: ListReply;

    constructor(type: string, button_reply: ButtonReply, list_reply: ListReply) {
        this.type = type;
        this.button_reply = button_reply;
        this.list_reply = list_reply;
    }

}

export class ButtonReply {

    readonly id: string;
    readonly title: string;

    constructor(id: string, title: string) {
        this.id = id;
        this.title = title;
    }

}

export class ListReply {

    readonly id: string;
    readonly title: string;
    readonly description: string;

    constructor(id: string, title: string, description: string) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

}
