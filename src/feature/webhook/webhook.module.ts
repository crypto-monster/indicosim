import { Module } from "@nestjs/common";
import { WebhookController } from "./controller/webhook.controller";
import { WhatsappModule } from "../whatsapp/whatsapp.module";
import { WebhookService } from "./service/webhook.service";
import { ChatTextService } from "./service/chat.text.service";
import { SearchModule } from "../search/search.module";
import { MessageListButtonService } from "./service/message.list.button.service";
import { MessageButtonService } from "./service/message.button.service";
import { UserIndicosimModule } from "../user-indicosim/user.indicosim.module";
import { CategoryModule } from "../category/category.module";
import { ProfessionalModule } from "../professional/professional.module";
import { CityModule } from "../city/city.module";

@Module({
    imports: [
        WhatsappModule,
        SearchModule,
        UserIndicosimModule,
        CategoryModule,
        ProfessionalModule,
        CityModule
    ],
    providers: [
        WebhookService,
        ChatTextService,
        MessageListButtonService,
        MessageButtonService
    ],
    controllers: [
        WebhookController
    ],
    exports: []
})
export class WebHookModule { }
