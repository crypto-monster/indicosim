import { Controller, Get, HttpCode, Post, Query, Body } from '@nestjs/common';
import { WhatsappClient } from "../../whatsapp/client/whatsapp.client";
import { EncodeSoundex } from "../../../common/soundex/encode.soundex";
import {
    ActionMessage,
    BodyMessage,
    ButtonMessage,
    ContactMessage,
    FooterMessage,
    HeaderMessage,
    InteractiveMessage,
    NameMessage,
    PhoneMessage,
    ReplyMessage,
    RowMessage,
    Message,
    SectionMessage,
    TextMessage,
    UrlMessage
} from "../../whatsapp/model";
import {MessageReceive} from "../dto/message.receive";
import {DefineTypeOfMessage} from "../../../common/util/define.type.of.message";
import {WebhookService} from "../service/webhook.service";

@Controller('webhook')
export class WebhookController {

    constructor(
        private readonly webHookService: WebhookService
    ) {}

    @Get()
    @HttpCode(200)
    findAll(@Body() message: string, @Query('hub.challenge') hubChanllenge: string,
        @Query('hub.verify_token') verifyToken: string): string {

        if("91f59456-676d-4722-ab3b-bca202b5830a" != verifyToken) {
            throw new Error("Error");
        }

        console.log('GET: ', JSON.stringify(message))
        return hubChanllenge;
    }

    @Post()
    @HttpCode(200)
    post(@Body() message: MessageReceive) {
        this.webHookService.message(message);

        return 'EVENT_RECEIVED';
    }

    @Get('/soundex')
    @HttpCode(200)
    poste(@Query('message') message: string): string {
        return EncodeSoundex.encode(message.toUpperCase());
    }
}
