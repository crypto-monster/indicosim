export enum InterestArea {
    HOME = "Para o meu lar",
    CAR = "Para o meu carro",
}

export function convertValueToLastMessageType(value: string) : InterestArea {
    switch (value) {
        case "Para o meu lar": {
            return InterestArea.HOME;
        }
        case "Para o meu carro": {
            return InterestArea.CAR;
        }
        default: {
            throw new Error('InterestArea does not exists');
        }
    }
}