import { Module } from "@nestjs/common";
import { UserIndicosimRepository } from "./repository/user.indicosim.repository";
import { UserIndicosimService } from "./service/user.indicosim.service";
import { PrismaService } from "../../config/prisma/prisma.service";

@Module({
    imports: [],
    providers: [
        UserIndicosimRepository,
        UserIndicosimService,
        PrismaService,
    ],
    exports: [
        UserIndicosimService
    ],
    controllers: []
})
export class UserIndicosimModule { }
