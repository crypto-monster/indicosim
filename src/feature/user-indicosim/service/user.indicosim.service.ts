import { Injectable } from "@nestjs/common";
import { UserIndicosimRepository } from "../repository/user.indicosim.repository";
import { UserIndicosim } from "../model/user.indicosim.model";

@Injectable()
export class UserIndicosimService {

    constructor(
        private readonly userIndicosimRepository: UserIndicosimRepository
    ) {}

    async save(userIndicosim: UserIndicosim): Promise<void> {
        this.userIndicosimRepository.save(userIndicosim);
    }


    async findByPhoneNumberAndCreatedDateAndTimeWindowChat(phoneNumber: string): Promise<UserIndicosim[]> {
        return this.userIndicosimRepository.findByPhoneNumberAndCreatedDateAndTimeWindowChat(phoneNumber);
    }

}