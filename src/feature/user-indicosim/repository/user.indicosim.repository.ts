import { Injectable } from "@nestjs/common";
import 'moment/locale/pt-br';

import { PrismaService } from "../../../config/prisma/prisma.service";
import {
    addHours,
    format
} from "date-fns";
import {
    UserIndicosim,
    UserIndicosimBD
} from "../model/user.indicosim.model";

@Injectable()
export class UserIndicosimRepository {

    constructor(
        private readonly prismaService: PrismaService
    ) { }

    async save(userIndicosim: UserIndicosim): Promise<void> {
        await this.prismaService.user_indicosim.upsert({
            where: {
                uuid: userIndicosim.uuid,
            },
            update: {
                phone_number: userIndicosim.phoneNumber,
                created_date: userIndicosim.createdDate,
                last_message: userIndicosim.lastMessage,
                last_message_type: userIndicosim.lastMessageType,
                time_window_chat: userIndicosim.timeWindowChat,
                city_uuid: userIndicosim.cityUuid
            },
            create: {
                uuid: userIndicosim.uuid,
                phone_number: userIndicosim.phoneNumber,
                created_date: userIndicosim.createdDate,
                last_message: userIndicosim.lastMessage,
                last_message_type: userIndicosim.lastMessageType,
                time_window_chat: userIndicosim.timeWindowChat,
                city_uuid: userIndicosim.cityUuid
            }
        })
    }

    async findByPhoneNumberAndCreatedDateAndTimeWindowChat(phoneNumber: string): Promise<UserIndicosim[]> {
        const now = new Date();
        const date = format(now, 'yyyy-MM-dd');
        const currentDateTime = format(now, 'yyyy-MM-dd HH:mm:ss');
        const dateTime24HoursBefore = format(addHours(now, -24), 'yyyy-MM-dd HH:mm:ss');

        const userIndicosim = await this.prismaService.$queryRaw<UserIndicosimBD[]>`
            select ui.* 
            from user_indicosim ui 
            left join city c on c.uuid = ui.city_uuid 
            where ui.created_date = ${date}::text and 
            ui.phone_number = ${phoneNumber}::text and 
            ui.time_window_chat between ${dateTime24HoursBefore}::text and ${currentDateTime}::text
        `;

        return userIndicosim.map(ui =>
            new UserIndicosim(
                ui.uuid,
                ui.phone_number,
                ui.created_date,
                ui.last_message,
                ui.last_message_type,
                ui.time_window_chat,
                ui.city_uuid
            )
        )
    }

}