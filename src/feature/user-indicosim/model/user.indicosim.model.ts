export class UserIndicosimBD {

    readonly uuid: string;
    readonly phone_number: string;
    readonly created_date: string;
    readonly last_message: string;
    readonly last_message_type: string;
    readonly time_window_chat: string;
    readonly city: any;
    readonly city_uuid: string;

}

export class UserIndicosim {

    readonly uuid: string;
    readonly phoneNumber: string;
    readonly createdDate: string;
    readonly lastMessage: string;
    lastMessageType: string;
    readonly timeWindowChat: string;
    cityUuid: string;

    constructor(
        uuid: string,
        phoneNumber: string,
        createdDate: string,
        lastMessage: string,
        lastMessageType: string,
        timeWindowChat: string,
        cityUuid: string
    ) {
        this.uuid = uuid;
        this.phoneNumber = phoneNumber;
        this.createdDate = createdDate;
        this.lastMessage = lastMessage;
        this.lastMessageType = lastMessageType;
        this.timeWindowChat = timeWindowChat;
        this.cityUuid = cityUuid;
    }
}