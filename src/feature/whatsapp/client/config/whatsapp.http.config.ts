import { Injectable } from '@nestjs/common';
import {AbstractHttpClientConfig} from "../../../../config/config.client";

@Injectable()
export class WhatsappHttpConfig extends AbstractHttpClientConfig {

    protected getHeaders(): Record<string, string> {
        let headers = super.getHeaders();

        headers = this.setHeaderAuthorization(headers);

        return headers;
    }

    protected setHeaderAuthorization(headers) {
    console.log(this.configService.get('BEARER_TOKEN_WHATSAPP'))
        return {
        ...headers,
            'Authorization': `Bearer ${this.configService.get('BEARER_TOKEN_WHATSAPP')}`
        }
    }

    protected getHost(): string {
        return this.configService.get('WHATSAPP_URL');
    }

    protected getTimeout(): number {
        return +this.configService.get('WHATSAPP_CLIENT_HTTP_READ_TIMEOUT_MILLIS');
    }

}
