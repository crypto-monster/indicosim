export class Response {

    messaging_product: string;
    contacts: ContactResponseMessage[];
    messages: MessageResponse[];

    constructor(messaging_product: string, contacts: ContactResponseMessage[], messages: MessageResponse[]) {
        this.messaging_product = messaging_product;
        this.contacts = contacts;
        this.messages = messages;
    }

}

export class ContactResponseMessage {

    input: string;
    wa_id: string;

    constructor(input: string, wa_id: string) {
        this.input = input;
        this.wa_id = wa_id;
    }

}

export class MessageResponse {

    id: string;

    constructor(id: string) {
        this.id = id;
    }

}