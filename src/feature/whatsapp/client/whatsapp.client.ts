import { Injectable } from "@nestjs/common";
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from "rxjs";
import { Message } from "../model";
import { Response } from "./model/response.message";

@Injectable()
export class WhatsappClient {

    constructor(
        private readonly httpService: HttpService
    ) { }

    async sendMessage(message: Message): Promise<Response> {
        const pipe = this.httpService.post('/v15.0/108277192147694/messages', message);

        return lastValueFrom(pipe)
            .then(response => response.data)
            .catch(error => {
                console.log(error)
                throw error
            });
    }

}