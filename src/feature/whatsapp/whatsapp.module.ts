import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { WhatsappHttpConfig } from "./client/config/whatsapp.http.config";
import { WhatsappClient } from "./client/whatsapp.client";

@Module({
    imports: [
        HttpModule.registerAsync({useClass: WhatsappHttpConfig})
    ],
    providers: [
        WhatsappClient
    ],
    exports: [
        WhatsappClient
    ]
})
export class WhatsappModule { }
