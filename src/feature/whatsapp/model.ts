export class Message {

    constructor(to: string, type: string, text?: TextMessage, interactive?: InteractiveMessage, action?: ActionMessage, contacts?: ContactMessage[]) {
        this.to = to;
        this.type = type;
        this.text = text;
        this.interactive = interactive;
        this.action = action;
        this.contacts = contacts;
    }

    messaging_product: string = "whatsapp";
    recipient_type: string =  "individual";
    to: string;
    type: string;
    text: TextMessage
    interactive: InteractiveMessage;
    action: ActionMessage;
    contacts: ContactMessage[];
}

export class ContactMessage {

    constructor(name: NameMessage, phones: PhoneMessage[], urls: UrlMessage[]) {
        this.name = name;
        this.phones = phones;
        this.urls = urls;
    }

    name: NameMessage;
    phones: PhoneMessage[];
    urls: UrlMessage[];
}

export class NameMessage {

    constructor(formatted_name: string, first_name: string, last_name: string) {
        this.formatted_name = formatted_name;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    formatted_name: string;
    first_name: string;
    last_name: string;
}

export class PhoneMessage {

    constructor(phone: string, wa_id: string, type: string) {
        this.phone = phone;
        this.wa_id = wa_id;
        this.type = type;
    }

    phone: string;
    wa_id: string;
    type: string;
}

export class UrlMessage {

    constructor(url: string, type: string) {
        this.url = url;
        this.type = type;
    }

    url: string;
    type: string;
}

export class TextMessage {

    constructor(previewUrl: boolean, body: string) {
        this.preview_url = previewUrl;
        this.body = body;
    }

    preview_url: boolean;
    body: string;
}

export class InteractiveMessage {

    constructor(type: string, body: BodyMessage, action: ActionMessage, footer?: FooterMessage, header?: HeaderMessage) {
        this.type = type;
        this.body = body;
        this.action = action;
        this.footer = footer;
    }

    type: string;
    body: BodyMessage
    action: ActionMessage;
    header: HeaderMessage;
    footer: FooterMessage;
}

export class HeaderMessage {

    constructor(text: string) {
        this.text = text;
    }

    type: string = 'text';
    text: string;

}

export class FooterMessage {


    constructor(text: string) {
        this.text = text;
    }

    text: string;

}

export class BodyMessage {

    constructor(text: string) {
        this.text = text;
    }

    text: string;
}

export class ActionMessage {


    constructor(buttons?: ButtonMessage[], button?: string, sections?: SectionMessage[]) {
        this.buttons = buttons;
        this.button = button;
        this.sections = sections;
    }

    buttons: ButtonMessage[];
    button: string;
    sections: SectionMessage[];
}

export class SectionMessage {

    constructor(title: string, rows: RowMessage[]) {
        this.rows = rows;
        this.title = title;
    }

    title: string;
    rows: RowMessage[]
}

export class RowMessage {

    constructor(id: string, title: string, description: string) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    id: string;
    title: string;
    description: string;
}

export class ButtonMessage {


    constructor(type: string, reply: ReplyMessage) {
        this.type = type;
        this.reply = reply;
    }

    type: string;
    reply: ReplyMessage
}

export class ReplyMessage {


    constructor(id: string, title: string) {
        this.id = id;
        this.title = title;
    }

    id: string;
    title: string;
}
