export class LettersRepresentsNumber {

    static letterH = 'H';

    static letterW = 'W';

    static lettersDiscarded: Array<string> = [
        'A', 'Á', 'À', 'Ã', 'Â',
        'E', 'É', 'È', 'Ê',
        'I', 'Í', 'Ì', 'Î',
        'O', 'Ó', 'Ò', 'Õ', 'Ô',
        'U', 'Ú', 'Ù', 'Û',
        'H',
        'W',
        'Y'];

    static letterNumber: Map<String, Number> = new Map<String, Number>([
        ['B', 1],
        ['F', 1],
        ['P', 1],
        ['V', 1],
        ['C', 2],
        ['G', 2],
        ['J', 2],
        ['K', 2],
        ['Q', 2],
        ['S', 2],
        ['X', 2],
        ['Z', 2],
        ['D', 3],
        ['T', 3],
        ['L', 4],
        ['M', 5],
        ['N', 5],
        ['R', 6],
    ])
    static characterSpecial: Array<string> = ['!', '@', '#', '$', '%', '¨', '&', '*', '(', ')', '_', '=', '+', '-', ';', ',', '.'];

}