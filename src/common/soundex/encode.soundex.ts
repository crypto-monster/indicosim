import { LettersRepresentsNumber } from "./letters.represents.number";

export class EncodeSoundex {

    public static encode(word: string): string {
        try {
            return this.converter(word.toUpperCase());
        } catch (error) {
            throw new Error('Error when generate code soundex');
        }
    }

    private static converter(word: string): string {
        const firstLetter = String(word).charAt(0);

        const wordSize = word.length;
        let codeGenerated: Array<string> = new Array<string>();
        let previousLetter = '';

        for (let i = 1; i < wordSize; i++) {
            const currentLetter = String(word).charAt(i);

            if (this.isCharacterIsASpace(currentLetter) || this.isACharacterSpecial(currentLetter)) {
                previousLetter = currentLetter;
                continue;
            }

            if (this.codeAlreadyHasNumberCharactersByArray(codeGenerated, 3)) {
                previousLetter = currentLetter;
                continue;
            }

            if (this.currentLetterBeDiscarded(currentLetter)) {
                previousLetter = currentLetter;
                continue;
            }

            if (this.previousLetterEqualCurrentLetter(previousLetter, currentLetter)) {
                previousLetter = currentLetter;
                continue;
            }

            if (this.numberPreviousLetterEqualNumberCurrentLetter(previousLetter, currentLetter)) {
                previousLetter = currentLetter;
                continue;
            }

            if (
                this.numberPreviousLetterEqualNumberCurrentLetter(previousLetter, currentLetter) &&
                this.currentLetterBeDiscarded(currentLetter) &&
                this.numberNextLetterEqualNumberCurrentLetter(word, i, currentLetter)
            ) {
                codeGenerated.push(LettersRepresentsNumber.letterNumber.get(String(word).charAt(i + 1)).toString());
                previousLetter = currentLetter;
                continue;
            } else {
                codeGenerated.push(LettersRepresentsNumber.letterNumber.get(currentLetter).toString());
                previousLetter = currentLetter;
            }

            if (
                this.currentLetterIsEqual(currentLetter, LettersRepresentsNumber.letterH) ||
                this.currentLetterIsEqual(currentLetter,  LettersRepresentsNumber.letterW) &&
                this.numberNextLetterEqualNumberCurrentLetter(word, i, previousLetter)
            ) {
                codeGenerated.push(LettersRepresentsNumber.letterNumber.get(String(word).charAt(i + 1)).toString());
                previousLetter = currentLetter;
                continue;
            }

            previousLetter = currentLetter;
        }

        return this.generateCodeSoundex(firstLetter, codeGenerated);
    }

    private static currentLetterIsEqual(letterCurrent: string, letterComparing: string) {
        return letterCurrent === letterComparing;
    }

    private static isCharacterIsASpace(letterCurrent) {
        return letterCurrent === " ";
    }

    private static generateCodeSoundex(firstLetter: string, code: Array<string>): string {
        let newCode = null;

        if (this.codeAlreadyHasNumberCharactersByArray(code, 1)) {
            newCode = code[0] + '00'
        }

        if (this.codeAlreadyHasNumberCharactersByArray(code, 2)) {
            newCode = code[0] + code[1] + '0'
        }

        return firstLetter + '-' + this.decideUseCode(newCode, code.toString());
    }

    private static decideUseCode(newCode: string, code: string) {
        return newCode != null ? newCode : code.replaceAll(',', '');
    }

    private static codeAlreadyHasNumberCharactersByArray(code: Array<string>, numberExpected: number): boolean {
        return code.length == numberExpected;
    }

    private static currentLetterBeDiscarded(letterCurrent: string): boolean {
        return LettersRepresentsNumber.lettersDiscarded.includes(letterCurrent)
    }

    private static previousLetterEqualCurrentLetter(previousLetter: string, currentLetter: string): boolean {
        return previousLetter === currentLetter;
    }

    private static numberPreviousLetterEqualNumberCurrentLetter(previousLetter: string, currentLetter: string): boolean {
        return LettersRepresentsNumber.letterNumber.get(currentLetter) === LettersRepresentsNumber.letterNumber.get(previousLetter)
    }

    private static numberNextLetterEqualNumberCurrentLetter(word: string, i: number, letter: string): boolean {
        return LettersRepresentsNumber.letterNumber.get(String(word).charAt(i + 1)) === LettersRepresentsNumber.letterNumber.get(letter)
    }

    private static isACharacterSpecial(currentLetter: string) {
        return LettersRepresentsNumber.characterSpecial.includes(currentLetter);
    }
}