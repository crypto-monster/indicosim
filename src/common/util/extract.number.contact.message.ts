import {MessageReceive} from "../../feature/webhook/dto/message.receive";

export class ExtractNumberContactMessage {

    public static extract(message: MessageReceive): string {
        try {
            return message.entry[0].changes[0].value.contacts[0].wa_id;
        } catch (e) {
            //throw new Error('Error on extract number contact message');
            console.log('Error on extract number contact message'+e)
        }
    }

}