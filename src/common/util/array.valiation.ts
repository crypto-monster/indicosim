export class ArrayValidation {
    public static isEmpty(any: any) {
      return Array.isArray(any) && !any.length;
    }
  }
  