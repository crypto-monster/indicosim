import { MessageReceive } from "../../feature/webhook/dto/message.receive";

export class ExtractTextMessage {

    public static extract(message: MessageReceive) {
        return message.entry[0].changes[0].value.messages[0].text.body;
    }

}