import { MessageReceive } from "../../feature/webhook/dto/message.receive";

export class DefineTypeOfMessage {

    public static define(message: MessageReceive): string {
        try {
            if (this.isMessageText(message)) {
                return 'message-text';
            } else if (this.isMessageListButton(message)) {
                return 'message-list-button';
            } else if (this.isMessageButton(message)) {
                return 'message-button';
            } else {
                return 'unidentified-message'
            }
        } catch (e) {
            //return 'unidentified message';
            console.log("unidentified message"+e);
        }
    }

    private static isMessageText(message: MessageReceive): boolean {
        try {
            if (!!message.entry[0].changes[0].value.messages[0].text?.body) {
                return true;
            }

            return false;
        } catch (e) {
            throw e;
        }
    }

    private static isMessageListButton(message: MessageReceive): boolean {
        try {
            if (message.entry[0].changes[0].value.messages[0].interactive?.type === 'list_reply') {
                return true;
            }

            return false;
        } catch (e) {
            throw e;
        }
    }

    private static isMessageButton(message: MessageReceive): boolean {
        try {
            if (message.entry[0].changes[0].value.messages[0].interactive?.type === 'button_reply') {
                return true;
            }

            return false;
        } catch (e) {
            throw e;
        }
    }

}