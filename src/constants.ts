export const SEQUELIZE = 'SEQUELIZE';
export const DEVELOPMENT = 'development';
export const TEST = 'test';
export const PRODUCTION = 'production';
export const CLUE_SERVICE_TAG_REPOSITORY = 'CLUE_SERVICE_TAG_REPOSITORY';