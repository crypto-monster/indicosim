import { ChatTextService } from "../../../../src/feature/webhook/service/chat.text.service";
import { WhatsappClient } from "../../../../src/feature/whatsapp/client/whatsapp.client";
import { SearchService } from "../../../../src/feature/search/service/search.service";
import { UserIndicosimService } from "../../../../src/feature/user-indicosim/service/user.indicosim.service";
import { CategoryService } from "../../../../src/feature/category/service/category.service";
import { ProfessionalService } from "../../../../src/feature/professional/service/professional.service";
import { CityService } from "../../../../src/feature/city/service/city.service";
import { CityRepository } from "../../../../src/feature/city/repository/city.repository";
import { PrismaService } from "../../../../src/config/prisma/prisma.service";
import { ProfessionalRepository } from "../../../../src/feature/professional/repository/professional.repository";
import { CategoryRepository } from "../../../../src/feature/category/repository/category.repository";
import { UserIndicosimRepository } from "../../../../src/feature/user-indicosim/repository/user.indicosim.repository";
import { SearchRepository } from "../../../../src/feature/search/repository/search.repository";
import { HttpService } from "@nestjs/axios";
import {
    Change,
    Contact,
    Entry,
    MessageReceive,
    Metadata,
    Profile,
    Text,
    Value,
    Message
} from "../../../../src/feature/webhook/dto/message.receive";
import { UserIndicosim } from "../../../../src/feature/user-indicosim/model/user.indicosim.model";
import { Search } from "../../../../src/feature/search/model/search.model";
import { Category } from "../../../../src/feature/category/model/category.model";
import { Professional } from "../../../../src/feature/professional/model/professional.model";
import {
    ContactResponseMessage,
    MessageResponse,
    Response
} from "../../../../src/feature/whatsapp/client/model/response.message";
import { City } from "../../../../src/feature/city/model/city.model";

describe('ChatTextService', () => {
    let chatTextService: ChatTextService;
    let whatsappClient: WhatsappClient;
    let searchService: SearchService;
    let userIndicoSimService: UserIndicosimService;
    let categoryService: CategoryService;
    let professionalService: ProfessionalService;
    let cityService: CityService;
    let cityRepository: CityRepository;
    let prismaService: PrismaService;
    let professionalRepository: ProfessionalRepository;
    let categoryRepository: CategoryRepository;
    let userIndicoSimRepository: UserIndicosimRepository;
    let searchRepository: SearchRepository;
    let httpService: HttpService;

    beforeEach(() => {
        prismaService = new PrismaService();
        whatsappClient = new WhatsappClient(httpService);
        searchRepository = new SearchRepository(prismaService);
        searchService = new SearchService(searchRepository);
        userIndicoSimRepository = new UserIndicosimRepository(prismaService);
        userIndicoSimService = new UserIndicosimService(userIndicoSimRepository);
        categoryRepository = new CategoryRepository(prismaService);
        categoryService = new CategoryService(categoryRepository);
        professionalRepository = new ProfessionalRepository(prismaService);
        professionalService = new ProfessionalService(professionalRepository);
        cityRepository = new CityRepository(prismaService);
        cityService = new CityService(cityRepository);
        chatTextService = new ChatTextService(whatsappClient, searchService, userIndicoSimService, categoryService, professionalService, cityService);
    });

    it('success', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            '0c80ce06-c77e-11ed-afa1-0242ac120002'
        )]));

        jest.spyOn(
            searchService,
            'findBySoundexNameAndEntityType'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', '0c80ce06-c77e-11ed-afa1-0242ac120002', 'SERVICE', 'Encanador', 'E-525', '0c80ce06-c77e-11ed-afa1-0242ac120003'
        )));

        jest.spyOn(
            searchService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120003', '0c80ce06-c77e-11ed-afa1-0242ac120001', 'CATEGORY', 'Hidraulica', 'H-200', null
        )));

        jest.spyOn(
            categoryService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Category(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', null, 'Hidraulica', 'H-200', true, '2023-01-01 00:00:00', '2023-01-01 00:00:00'
        )));

        jest.spyOn(
            professionalService,
            'findByCategory'
        ).mockImplementation(() => Promise.resolve([new Professional(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', 'Profissional 1', 'Profissional 1', '5544991535262', 'Rua sergipe 455', '11111111111', 'https://google.com', false, '2023-01-01 00:00:00', '2023-01-01 00:00:00'
        )]));

        jest.spyOn(
            chatTextService,
            'sendMessageProfessionals'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('chat null', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([]));

        jest.spyOn(
            cityService,
            'findAll'
        ).mockImplementation(() => Promise.resolve([
            new City('0c80ce06-c77e-11ed-afa1-0242ac120001', 'Maringá', 'M-200', '44', 'BR', '2023-01-01 00:00:00', '2023-01-01 00:00:00'),
            new City('0c80ce06-c77e-11ed-afa1-0242ac120002', 'Loanda', 'l-200', '44', 'BR', '2023-01-01 00:00:00', '2023-01-01 00:00:00'),
        ]));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('should - city_uuid null', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            null
        )]));

        jest.spyOn(
            cityService,
            'findAll'
        ).mockImplementation(() => Promise.resolve([
            new City('0c80ce06-c77e-11ed-afa1-0242ac120001', 'Maringá', 'M-200', '44', 'BR', '2023-01-01 00:00:00', '2023-01-01 00:00:00'),
            new City('0c80ce06-c77e-11ed-afa1-0242ac120002', 'Loanda', 'l-200', '44', 'BR', '2023-01-01 00:00:00', '2023-01-01 00:00:00'),
        ]));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('search null', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            '0c80ce06-c77e-11ed-afa1-0242ac120002'
        )]));

        jest.spyOn(
            searchService,
            'findBySoundexNameAndEntityType'
        ).mockImplementation(() => Promise.resolve(null));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('category null', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            '0c80ce06-c77e-11ed-afa1-0242ac120002'
        )]));

        jest.spyOn(
            searchService,
            'findBySoundexNameAndEntityType'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', '0c80ce06-c77e-11ed-afa1-0242ac120002', 'SERVICE', 'Encanador', 'E-525', '0c80ce06-c77e-11ed-afa1-0242ac120003'
        )));

        jest.spyOn(
            searchService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120003', '0c80ce06-c77e-11ed-afa1-0242ac120001', 'CATEGORY', 'Hidraulica', 'H-200', null
        )));

        jest.spyOn(
            categoryService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(null));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('category hasProfessional', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            '0c80ce06-c77e-11ed-afa1-0242ac120002'
        )]));

        jest.spyOn(
            searchService,
            'findBySoundexNameAndEntityType'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', '0c80ce06-c77e-11ed-afa1-0242ac120002', 'SERVICE', 'Encanador', 'E-525', '0c80ce06-c77e-11ed-afa1-0242ac120003'
        )));

        jest.spyOn(
            searchService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120003', '0c80ce06-c77e-11ed-afa1-0242ac120001', 'CATEGORY', 'Hidraulica', 'H-200', null
        )));

        jest.spyOn(
            categoryService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Category(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', null, 'Hidraulica', 'H-200', false, '2023-01-01 00:00:00', '2023-01-01 00:00:00'
        )));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });

    it('professional null', async () => {
        jest.spyOn(
            userIndicoSimService,
            'findByPhoneNumberAndCreatedDateAndTimeWindowChat'
        ).mockImplementation(() => Promise.resolve([new UserIndicosim(
            '0c80ce06-c77e-11ed-afa1-0242ac120002',
            '5544991535262',
            '2023-01-01 00:00:00',
            '2023-01-01 00:00:00',
            'Last message',
            '2023-01-01 23:59:00',
            null,
            '0c80ce06-c77e-11ed-afa1-0242ac120002'
        )]));

        jest.spyOn(
            searchService,
            'findBySoundexNameAndEntityType'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', '0c80ce06-c77e-11ed-afa1-0242ac120002', 'SERVICE', 'Encanador', 'E-525', '0c80ce06-c77e-11ed-afa1-0242ac120003'
        )));

        jest.spyOn(
            searchService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Search(
            '0c80ce06-c77e-11ed-afa1-0242ac120003', '0c80ce06-c77e-11ed-afa1-0242ac120001', 'CATEGORY', 'Hidraulica', 'H-200', null
        )));

        jest.spyOn(
            categoryService,
            'findByUuid'
        ).mockImplementation(() => Promise.resolve(new Category(
            '0c80ce06-c77e-11ed-afa1-0242ac120001', null, 'Hidraulica', 'H-200', true, '2023-01-01 00:00:00', '2023-01-01 00:00:00'
        )));

        jest.spyOn(
            professionalService,
            'findByCategory'
        ).mockImplementation(() => Promise.resolve([]));

        jest.spyOn(
            whatsappClient,
            'sendMessage'
        ).mockImplementation(() => Promise.resolve(new Response(
            'messaging_product: string',
            [new ContactResponseMessage('input: string', 'wa_id: string')],
            [new MessageResponse('1')]
        )));

        await chatTextService.message(
            new MessageReceive('whatsapp_business_account', [new Entry(
                '104119619235565', [new Change(
                    new Value(
                        'whatsapp',
                        new Metadata('15550067061', '108277192147694'),
                        [new Contact(
                            new Profile('Matheus Vieira Faxina'), '5544991535262'
                        )],
                        [new Message(
                            '5544991535262',
                            'wamid.HBgMNTU0NDk4OTIyOTM0FQIAEhggOUVEQTU3ODRDNTMyODAwNkQ0NjFFOEI1Q0QyQjdGQzEA',
                            '1677198804',
                            new Text('ENCANADOR'),
                            'text',
                            null,
                            null
                        )]
                    ), 'messages'
                )]
            )])
        );
    });
});