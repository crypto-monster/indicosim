import {EncodeSoundex} from "../../src/common/soundex/encode.soundex";

describe('EncodeSoundex', () => {
  let encodeSoundex: EncodeSoundex;

  beforeEach(async () => {
      encodeSoundex = new EncodeSoundex()
  });

  describe('encode', () => {

    it('should return the code A-343', () => {
      expect(encodeSoundex.encode("AUTOELETRICA")).toBe('A-343');
    });

    it('should return the code A-343 whith character special', () => {
      expect(encodeSoundex.encode("AUTO-ELETRICA")).toBe('A-343');
    });

    it('should return the code E-125', () => {
      expect(encodeSoundex.encode("PEDREIRO")).toBe('P-366');
    });

    it('should return the code E-125', () => {
      expect(encodeSoundex.encode("PEDREIRO")).toBe('P-366');
    });

    it('should return the code E-125 word wrong', () => {
      expect(encodeSoundex.encode("PEDRERO")).toBe('P-366');
    });

    it('should return the code E-525', () => {
      expect(encodeSoundex.encode("ENCANADOR")).toBe('E-525');
    });

    it('should return the code E-525 with space', () => {
      expect(encodeSoundex.encode("ENCA NADOR")).toBe('E-525');
    });

    it('should return the code E-240', () => {
      expect(encodeSoundex.encode("ASSOALHO")).toBe('A-240');
    });

    it('should return the code A-656', () => {
      expect(encodeSoundex.encode("ARRUMAR")).toBe('A-656');
    });

    it('should return the code T-522', () => {
      expect(encodeSoundex.encode("Tymczak")).toBe('T-522');
    });

    it('should return the code A-226', () => {
      expect(encodeSoundex.encode("Ashcraft")).toBe('A-226');
    });

    it('should return the code T-522', () => {
      expect(encodeSoundex.encode("Tymczak")).toBe('T-522');
    });

    it('should add one zero at the end when finding only one code', () => {
      expect(encodeSoundex.encode("Age")).toBe('A-200');
    });

    it('should add two zeros at the end when finding only one code', () => {
      expect(encodeSoundex.encode("Ag")).toBe('A-200');
    });

    it('should return the code M-263', () => {
      expect(encodeSoundex.encode("MACORATTI")).toBe('M-263');
    });

    it('should return the code A-165', () => {
      expect(encodeSoundex.encode("ABRAM")).toBe('A-165');
    });

    it('should return the code C-100', () => {
      expect(encodeSoundex.encode("CHUVA")).toBe('C-100');
    });

  });

  describe('error', () => {

    it('should throw an exception when there is an error', () => {
      expect(() => encodeSoundex.encode(null)).toThrow(Error('Error when generate code soundex'));
    });

  });
});
