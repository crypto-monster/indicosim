/*
  Warnings:

  - Made the column `city_uuid` on table `user_indicosim` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "user_indicosim" DROP CONSTRAINT "user_indicosim_city_uuid_fkey";

-- DropIndex
DROP INDEX "user_indicosim_city_uuid_key";

-- AlterTable
ALTER TABLE "user_indicosim" ALTER COLUMN "city_uuid" SET NOT NULL;
