-- CreateTable
CREATE TABLE "search" (
    "uuid" TEXT NOT NULL,
    "entity_uuid" TEXT NOT NULL,
    "entity_type" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "soundex_name" TEXT NOT NULL,
    "father_uuid" TEXT,

    CONSTRAINT "search_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "reputation" (
    "uuid" TEXT NOT NULL,
    "star" TEXT NOT NULL,
    "message" TEXT NOT NULL,
    "last_modified_date" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,
    "professional_uuid" TEXT NOT NULL,

    CONSTRAINT "reputation_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "category" (
    "uuid" TEXT NOT NULL,
    "father_uuid" TEXT,
    "father_name" TEXT,
    "description" TEXT,
    "name" TEXT NOT NULL,
    "soundex_name" TEXT NOT NULL,
    "has_professional" BOOLEAN NOT NULL,
    "last_modified_date" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "category_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "professional" (
    "uuid" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "fantasy_name" TEXT NOT NULL,
    "phone_number" TEXT NOT NULL,
    "address" TEXT NOT NULL,
    "document_number" TEXT NOT NULL,
    "page_url" TEXT NOT NULL,
    "disabled" TEXT NOT NULL,
    "last_modified_date" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "professional_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "professional_category" (
    "category_uuid" TEXT NOT NULL,
    "professional_uuid" TEXT NOT NULL,

    CONSTRAINT "professional_category_pkey" PRIMARY KEY ("category_uuid","professional_uuid")
);

-- CreateTable
CREATE TABLE "professional_city" (
    "city_uuid" TEXT NOT NULL,
    "professional_uuid" TEXT NOT NULL,

    CONSTRAINT "professional_city_pkey" PRIMARY KEY ("city_uuid","professional_uuid")
);

-- CreateTable
CREATE TABLE "user_indicosim" (
    "uuid" TEXT NOT NULL,
    "phone_number" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,
    "last_message" TEXT NOT NULL,
    "last_message_type" TEXT NOT NULL,
    "time_window_chat" TEXT NOT NULL,
    "city_uuid" TEXT,

    CONSTRAINT "user_indicosim_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "city" (
    "uuid" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "soundex_name" TEXT NOT NULL,
    "ddd" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "last_modified_date" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "city_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "category_metric" (
    "uuid" TEXT NOT NULL,
    "client_uuid" TEXT NOT NULL,
    "user_indicosim_uuid" TEXT NOT NULL,
    "phone_number" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "category_metric_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "professional_metric" (
    "uuid" TEXT NOT NULL,
    "client_uuid" TEXT NOT NULL,
    "user_indicosim_uuid" TEXT NOT NULL,
    "phone_number" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "professional_metric_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "service_metric" (
    "uuid" TEXT NOT NULL,
    "client_uuid" TEXT NOT NULL,
    "user_indicosim_uuid" TEXT NOT NULL,
    "phone_number" TEXT NOT NULL,
    "created_date" TEXT NOT NULL,

    CONSTRAINT "service_metric_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE INDEX "search_soundex_name_entity_type_idx" ON "search"("soundex_name", "entity_type");

-- CreateIndex
CREATE UNIQUE INDEX "user_indicosim_city_uuid_key" ON "user_indicosim"("city_uuid");

-- CreateIndex
CREATE INDEX "category_metric_client_uuid_phone_number_created_date_idx" ON "category_metric"("client_uuid", "phone_number", "created_date");

-- CreateIndex
CREATE INDEX "professional_metric_client_uuid_phone_number_created_date_idx" ON "professional_metric"("client_uuid", "phone_number", "created_date");

-- CreateIndex
CREATE INDEX "service_metric_client_uuid_phone_number_created_date_idx" ON "service_metric"("client_uuid", "phone_number", "created_date");

-- AddForeignKey
ALTER TABLE "reputation" ADD CONSTRAINT "reputation_professional_uuid_fkey" FOREIGN KEY ("professional_uuid") REFERENCES "professional"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "professional_category" ADD CONSTRAINT "professional_category_category_uuid_fkey" FOREIGN KEY ("category_uuid") REFERENCES "category"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "professional_category" ADD CONSTRAINT "professional_category_professional_uuid_fkey" FOREIGN KEY ("professional_uuid") REFERENCES "professional"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "professional_city" ADD CONSTRAINT "professional_city_city_uuid_fkey" FOREIGN KEY ("city_uuid") REFERENCES "city"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "professional_city" ADD CONSTRAINT "professional_city_professional_uuid_fkey" FOREIGN KEY ("professional_uuid") REFERENCES "professional"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "user_indicosim" ADD CONSTRAINT "user_indicosim_city_uuid_fkey" FOREIGN KEY ("city_uuid") REFERENCES "city"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;
