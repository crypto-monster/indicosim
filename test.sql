INSERT INTO public.city (uuid, "name", soundex_name, ddd, country, last_modified_date, created_date) VALUES('0128b280-c459-11ed-afa1-0242ac120002', 'Maringá', 'M-100', '44', 'BR', '2023-03-03 11:11:11', '2023-03-03 11:11:11');

INSERT INTO public.user_indicosim (uuid, phone_number, created_date, last_message, time_window_chat, city_uuid, last_message_type) VALUES('0128b280-c459-11ed-afa1-0242ac120002', '554498922934', '2023-03-16', '1', '2023-03-16 20:20:20', '0128b280-c459-11ed-afa1-0242ac120002', 'city');

INSERT INTO category
(uuid, father_uuid, "name", soundex_name, has_professional, last_modified_date, created_date, father_name, description)
VALUES('b13b3ec1-a290-4451-89a0-b9b12f07f351', NULL, 'Serviços gerais', 'S-612;G-620', true, '2023-03-16 20:20:20', '2023-03-16 20:20:20', 'Para o meu lar', 'Trocar lampadas, torneiras entre outros');

INSERT INTO category
(uuid, father_uuid, "name", soundex_name, has_professional, last_modified_date, created_date, father_name, description)
VALUES('0acf777c-c31a-44b3-9aeb-67f064e25650', NULL, 'Montagem', 'M-532', true, '2023-03-16 20:20:20', '2023-03-16 20:20:20', 'Para o meu lar', 'Montar/Desmontar guarda-roupas, armários em geral...');

INSERT INTO professional
(uuid, "name", fantasy_name, phone_number, address, document_number, page_url, disabled, last_modified_date, created_date)
VALUES('7dff57d2-64aa-4528-aecc-eeef9f2e5fdb', 'Victor Molina de Avila', 'Avila', '5544998922934', 'Rua Henrique Natal Ridolgi, 229', '43216508870', 'https://www.facebook.com/victor.molinadeavila', 'false', '2023-03-16 20:20:20', '2023-03-16 20:20:20');


INSERT INTO professional_category
(category_uuid, professional_uuid)
VALUES('b13b3ec1-a290-4451-89a0-b9b12f07f351', '7dff57d2-64aa-4528-aecc-eeef9f2e5fdb');


INSERT INTO professional
(uuid, "name", fantasy_name, phone_number, address, document_number, page_url, disabled, last_modified_date, created_date)
VALUES('cd1bae52-c6bb-4f24-a8c1-219486bc73cb', 'Matheus Faxina', 'Faxina', '554491535262', 'Rua Henrique Natal Ridolgi, 229', '43216508870', 'https://www.facebook.com/matheus.faxina', 'false', '2023-03-16 20:20:20', '2023-03-16 20:20:20');


INSERT INTO professional_category
(category_uuid, professional_uuid)
VALUES('b13b3ec1-a290-4451-89a0-b9b12f07f351', 'cd1bae52-c6bb-4f24-a8c1-219486bc73cb');
